// Variables
const todos = [
    { id: 1, title: 'My first todo', completed: false },
    { id: 2, title: 'Go for a run', completed: false },
    { id: 3, title: 'Get some sleep', completed: false }
]

// DOM Elements
const elNewTodoTitle = document.getElementById('new-todo-title')
const elBtnAddTodo = document.getElementById('btn-add-todo')
const elTodoList = document.getElementById('todo-list')

// HTML events
elBtnAddTodo.addEventListener('click', handleAddTodoClick)

// Event Handlers
function handleAddTodoClick () {
    // Get the new todo value
    const newTodo = elNewTodoTitle.value.trim()
    // add it to the array
    todos.push({
        id: 4,
        title: newTodo,
        completed: false
    })
    // re-render the HTML
    render()

}
function render() {
    const elTodoItems = createTodosItems(todos)
    elTodoList.innerHTML = ''
    elTodoItems.forEach(function(todoItem){
        elTodoList.appendChild(todoItem)
    })
}

// Return list of HTML elements.
function createTodosItems(todos = []) {
    const elTodoItems = todos.map(function(todo){

        const elTodoItem = document.createElement('li')
        elTodoItem.innerText = todo.title
        
        return elTodoItem

    })
    return elTodoItems
}

render()


